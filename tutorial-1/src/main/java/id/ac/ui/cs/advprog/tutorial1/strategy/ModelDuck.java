package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    // TODO Complete me!
    public ModelDuck(FlyBehavior flybehavior, QuackBehavior quackbehavior) {

        super(flybehavior, quackbehavior);
    }

    public void display() {
        System.out.println("im modelduck");
    }
}
