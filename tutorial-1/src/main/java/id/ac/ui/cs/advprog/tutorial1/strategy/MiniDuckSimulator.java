package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MiniDuckSimulator {

    public static void main(String[] args) {

        FlyBehavior flynoway = new FlyNoWay();
        FlyBehavior flywithwings = new FlyWithWings();
        FlyBehavior flyrocket = new FlyRocketPowered();
        QuackBehavior quack = new Quack();
        QuackBehavior mute = new MuteQuack();
        QuackBehavior squeak = new Squeak();


        Duck mallard = new MallardDuck(flynoway, quack);
        mallard.performQuack();
        mallard.performFly();

        //todo fix me
        Duck model = new ModelDuck(flyrocket, squeak);
        model.performFly();
        model.swim();
        model.display();
        model.setFlyBehavior(flyrocket);
        model.performFly();

    }
}
